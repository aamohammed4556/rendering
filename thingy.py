import pygame
from numpy import array
from math import cos, sin
from random import randint
from pygame import K_q, K_w, K_a, K_s, K_z, K_x, K_UP, K_DOWN

X, Y, Z = 0, 1, 2

def rotation_matrix(a, B, γ):
    """
    rotation matrix of a, B, γ radians around x, y, z axes (respectively)
    """
    sa, ca = sin(a), cos(a)
    sB, cB = sin(B), cos(B)
    sγ, cγ = sin(γ), cos(γ)
    return (
        (cB * cγ, -cB * sγ, sB),
        (ca * sγ + sa * sB * cγ, ca * cγ - sγ * sa * sB, -cB * sa),
        (sγ * sa - ca * sB * cγ, ca * sγ * sB + sa * cγ, ca * cB),
    )

class Cube:
    def __init__(self, vertices, edges, opposite):
        self.__vertices = array(vertices)
        self.__edges = tuple(edges)
        self.__rotation = [0, 0, 0]
        self.opposite = True ## CHANGE TO MAKE THE CUBES TURN OPPOSITE DIRECTIONS
        self.color = (255, 255, 10) #if opposite else (255, 0, 0)

    def zoom(self, scale):
        """Zoom in or out"""
        self.__vertices = array([tuple((coord * scale for coord in point)) for point in self.__vertices])

    def rotate(self, axis, θ):
        """Add angle to given axis"""
        if self.opposite:
            θ = -θ
        self.__rotation[axis] += θ

    @property
    def lines(self):
        location = self.__vertices.dot(rotation_matrix(*self.__rotation))
        return ((location[v1], location[v2]) for v1, v2 in self.__edges)


BLACK, RED = (0, 0, 0), (255, 128, 128)


class Paint:
    def __init__(self, shapes, keys_handler):
        self.__shapes = shapes
        self.__keys_handler = keys_handler
        self.__size = 450, 450
        self.__clock = pygame.time.Clock()
        self.__screen = pygame.display.set_mode(self.__size)
        self.__mainloop()


    def __fit(self, vec):
        return [
            round(70 * coordinate + frame / 2)
            for coordinate, frame in zip(vec, self.__size)
        ]

    def __handle_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                exit()
        self.__keys_handler(pygame.key.get_pressed(), self.__shapes)

    def __draw_shapes(self, thickness=4):
        for shape in self.__shapes:
            for start, end in shape.lines:
                pygame.draw.line(
                    self.__screen,
                    shape.color,
                    self.__fit(start),
                    self.__fit(end),
                    thickness,
                )

    def __mainloop(self):
        while True:
            self.__handle_events()
            self.__screen.fill(BLACK)
            self.__draw_shapes()
            pygame.display.flip()
            self.__clock.tick(70)


def cube_generator(i):
    return Cube(
            vertices=(
                (i, i, i),
                (i, i, -i),
                (i, -i, i),
                (i, -i, -i),
                (-i, i, i),
                (-i, i, -i),
                (-i, -i, i),
                (-i, -i, -i),
            ),
            edges=(
                {0, 1},
                {0, 2},
                {2, 3},
                {1, 3},
                {4, 5},
                {4, 6},
                {6, 7},
                {5, 7},
                {0, 4},
                {1, 5},
                {2, 6},
                {3, 7}
            ),
            #color=(255, 255, randint(1, 255)),
            opposite = bool(0 if i % 2 == 0 else 1)
        ) 

def main():
    # NUMBER OF CUBES
    cubes = [cube_generator(i) for i in range(1, 11)]

    counter_clockwise = 0.05
    clockwise = -counter_clockwise

    params = {
        K_q: (X, clockwise),
        K_w: (X, counter_clockwise),
        K_a: (Y, clockwise),
        K_s: (Y, counter_clockwise),
        K_z: (Z, clockwise),
        K_x: (Z, counter_clockwise),
    }

    def keys_handler(keys, shapes):
        for key in params:
            if keys[key]:
                for shape in shapes:
                    shape.rotate(*params[key])
            if keys[K_UP]:
                for shape in shapes:
                    shape.zoom(0.99)
            if keys[K_DOWN]:
                for shape in shapes:
                    shape.zoom(100/99)

    pygame.init()
    Paint(cubes, keys_handler)


if __name__ == "__main__":
    main()
